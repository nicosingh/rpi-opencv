FROM resin/rpi-raspbian:stretch-20180214

MAINTAINER Nicolas Singh <nicolas.singh@gmail.com>

# Download required software
RUN apt-get update && \
  apt-get -y install build-essential cmake cmake-curses-gui qt5-default libqt5svg5-dev qtcreator wget unzip && \
  rm -rf /var/lib/apt/lists/*

# Download OpenCV
RUN wget -O /tmp/opencv-2.4.11.zip "http://downloads.sourceforge.net/project/opencvlibrary/opencv-unix/2.4.11/opencv-2.4.11.zip"
RUN unzip -d /tmp /tmp/opencv-2.4.11.zip

# Install OpenCV
RUN mkdir -p /tmp/opencv-2.4.11/build
WORKDIR /tmp/opencv-2.4.11/build
RUN cmake -DCMAKE_BUILD_TYPE=Release ..
RUN make -j4
RUN make install

# Remove installation files
RUN rm -rf /tmp/opencv-2.4.11
