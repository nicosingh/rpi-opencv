[![pipeline status](https://gitlab.com/nicosingh/rpi-opencv/badges/master/pipeline.svg)](https://gitlab.com/nicosingh/rpi-opencv/commits/master) [![Docker Pulls](https://img.shields.io/docker/pulls/nicosingh/rpi-opencv.svg)](https://hub.docker.com/r/nicosingh/rpi-opencv/)

# About

Docker image of Raspbian with OpenCV decoder already installed. It is made to avoid the lazyness of compiling OpenCV in a raspberry pi, that takes a lot of time to complete :)

# How to use this Docker image?

We can include it in another image, in our Dockerfile:

`FROM nicosingh/rpi-opencv`

Or use it to test OpenCV functionalities through a Bash shell, with something like:

`docker run -it nicosingh/rpi-opencv bash`

Where:

`docker run -it`: means to run a new Docker container with interactive mode

`nicosingh/rpi-opencv bash`: means to open a BASH shell in our new container
